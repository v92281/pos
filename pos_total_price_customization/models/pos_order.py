# -*- coding: utf-8 -*-
from odoo import models, api


class PosOrder(models.Model):

    _inherit = 'pos.order'

    def _prepare_invoice(self):
        res = super(PosOrder, self)._prepare_invoice()
        if self.session_id.config_id.ftotal_price_without_tax:
            res.update({'pos_ftotal_price_without_tax': True})
        return res


class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    @api.depends('price_unit', 'tax_ids', 'qty', 'discount', 'product_id')
    def _compute_amount_line_all(self):
        super(PosOrderLine, self)._compute_amount_line_all()
        for line in self:
            config_id = line.order_id.session_id.config_id
            fpos = line.order_id.fiscal_position_id
            tax_ids_after_fiscal_position = fpos.map_tax(
                line.tax_ids, line.product_id,
                line.order_id.partner_id) if fpos else line.tax_ids
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = tax_ids_after_fiscal_position.compute_all(
                price, line.order_id.pricelist_id.currency_id, line.qty,
                product=line.product_id, partner=line.order_id.partner_id)
            taxes_diff = taxes['total_included'] - taxes['total_excluded']
            tax_exclude = taxes['total_excluded'] - taxes_diff if \
                config_id.ftotal_price_without_tax else taxes['tax_exclude']
            line.update({
                'price_subtotal_incl': taxes['total_excluded'],
                'price_subtotal': tax_exclude,
            })

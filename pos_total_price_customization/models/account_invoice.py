from odoo import models, fields, api


class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    pos_ftotal_price_without_tax = fields.Boolean(
        string='From POS Total without Tax')

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.invoice_line_ids:
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            ctx = self._context.copy()
            ctx.update({'pos_ftotal_price_without_tax': True})
            if self.pos_ftotal_price_without_tax:
                taxes = line.invoice_line_tax_ids.with_context(
                    ctx).compute_all(
                        price_unit, self.currency_id, line.quantity,
                        line.product_id, self.partner_id)['taxes']
            else:
                taxes = line.invoice_line_tax_ids.compute_all(
                    price_unit, self.currency_id, line.quantity,
                    line.product_id, self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(
                    tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
        return tax_grouped


class AccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids',
                 'quantity', 'product_id', 'invoice_id.partner_id',
                 'invoice_id.currency_id', 'invoice_id.company_id',
                 'invoice_id.date_invoice', 'invoice_id.date')
    def _compute_price(self):
        super(AccountInvoiceLine, self)._compute_price()
        currency = self.invoice_id and self.invoice_id.currency_id or None
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        taxes = False
        if self.invoice_line_tax_ids:
            ctx = self._context.copy()
            ctx.update({'pos_ftotal_price_without_tax': True})
            taxes = self.invoice_line_tax_ids.with_context(ctx).compute_all(
                price, currency, self.quantity, product=self.product_id,
                partner=self.invoice_id.partner_id)
        price_subtotal = taxes['total_excluded'] if \
            taxes else self.quantity * price
        taxes_diff = taxes['total_included'] - taxes['total_excluded']
        new_sub_total = taxes['total_excluded'] - taxes_diff
        self.price_subtotal = new_sub_total if \
            self.invoice_id.pos_ftotal_price_without_tax else price_subtotal

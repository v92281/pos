from odoo import models


class AccountTax(models.Model):

    _inherit = 'account.tax'

    def _compute_amount(self, base_amount, price_unit, quantity=1.0,
                        product=None, partner=None):
        self.ensure_one()
        if self._context.get('pos_ftotal_price_without_tax'):
            return base_amount - (base_amount / (1 + self.amount / 100))
        else:
            return super(AccountTax, self)._compute_amount(
                base_amount, price_unit, quantity=quantity, product=product,
                partner=partner)

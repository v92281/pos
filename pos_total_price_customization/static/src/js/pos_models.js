odoo.define('pos_total_price_customization.pos_models', function(require){
"use strict";

var utils = require('web.utils');
var models = require('point_of_sale.models');
var round_pr = utils.round_precision;

var _super_order = models.Order.prototype;
models.Order = models.Order.extend({
    get_total_with_tax: function() {
        if (this.pos.config.ftotal_price_without_tax){
            return this.get_total_without_tax() - this.get_total_tax();
        }
        else{
            return this.get_total_without_tax() + this.get_total_tax();
        }
    },
    get_change: function(paymentline) {
        if (this.pos.config.ftotal_price_without_tax){
            if (!paymentline) {
                var change = this.get_total_paid() - this.get_total_without_tax();
            } else {
                var change = -this.get_total_without_tax(); 
                var lines  = this.paymentlines.models;
                for (var i = 0; i < lines.length; i++) {
                    change += lines[i].get_amount();
                    if (lines[i] === paymentline) {
                        break;
                    }
                }
            }
            return round_pr(Math.max(0,change), this.pos.currency.rounding);
        }
        else{
            return _super_order.get_change.apply(this,arguments);
        }
    },
    get_due: function(paymentline) {
        if (this.pos.config.ftotal_price_without_tax){
            if (!paymentline) {
                var due = this.get_total_without_tax() - this.get_total_paid();
            } else {
                var due = this.get_total_without_tax();
                var lines = this.paymentlines.models;
                for (var i = 0; i < lines.length; i++) {
                    if (lines[i] === paymentline) {
                        break;
                    } else {
                        due -= lines[i].get_amount();
                    }
                }
            }
            return round_pr(Math.max(0,due), this.pos.currency.rounding);
        }
        else{
            return _super_order.get_due.apply(this,arguments);
        }
    },
});

var _super_orderline = models.Orderline.prototype;
models.Orderline = models.Orderline.extend({

    get_price_with_tax: function(){
        if (this.pos.config.ftotal_price_without_tax){
            return this.get_all_prices().priceWithoutTax;
        }
        else{
            return this.get_all_prices().priceWithTax;
        }
    },
    compute_all: function(taxes, price_unit, quantity, currency_rounding, no_map_tax) {
        var self = this;
        var res = _super_orderline.compute_all.apply(this,arguments);
        if (this.pos.config.ftotal_price_without_tax){
            res.total_included = res.total_excluded;
        }
        return res;
    },
    _compute_all: function(tax, base_amount, quantity) {
        var self = this;
        if (this.pos.config.ftotal_price_without_tax){
            return base_amount - (base_amount / (1 + tax.amount / 100));
        }
        return _super_orderline._compute_all.apply(this,arguments);
    },
    get_order: function(){
        return this.order;
    },
    get_line_tax_details: function(){
        var details = {};
        var fulldetails = [];
        var ldetails = this.get_tax_details();
        for(var id in ldetails){
            if(ldetails.hasOwnProperty(id)){
                details[id] = (details[id] || 0) + ldetails[id];
            }
        }
        for(var id in details){
            if(details.hasOwnProperty(id)){
                fulldetails.push({amount: details[id], tax: this.pos.taxes_by_id[id], name: this.pos.taxes_by_id[id].name});
            }
        }

        return fulldetails;
    },

});

});

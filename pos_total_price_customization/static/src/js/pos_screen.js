odoo.define('pos_total_price_customization.pos_screen', function(require){
"use strict";

var screen = require('point_of_sale.screens');

var OrderWidget = screen.OrderWidget.include({

    update_summary: function(){
        var self = this;
        var order = this.pos.get_order();
        if (!order.get_orderlines().length) {
            return;
        }
        var total     = order ? order.get_total_with_tax() : 0;
        var taxes     = order ? total - order.get_total_without_tax() : 0;
        if (order.pos.config.ftotal_price_without_tax){
            taxes = taxes * -1;
        }
        this.el.querySelector('.summary .total > .value').textContent = this.format_currency(total);
        this.el.querySelector('.summary .total .subentry .value').textContent = this.format_currency(taxes);
        if (order.pos.config.ftotal_price_without_tax){
            this.el.querySelector('.summary .total .subentry_total .value').textContent = this.format_currency(total + taxes);
        }
    },

});

});
